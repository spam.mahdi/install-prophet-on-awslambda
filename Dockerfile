FROM python:3.8-slim
RUN apt-get -y update  && apt-get install -y \
	  python3-dev \
	  apt-utils \
	  python-dev \
	  build-essential \
          g++ \
          make \
          cmake \
          unzip \
          libcurl4-openssl-dev \
	&& rm -rf /var/lib/apt/lists/*

COPY Dockerfile .
COPY requirements.txt .

RUN pip install awslambdaric
RUN pip3 install -r requirements.txt --target "${LAMBDA_TASK_ROOT}"


ENTRYPOINT [ "/usr/local/bin/python3", "-m", "awslambdaric" ]
CMD ["FS_Main_meal.handler"]